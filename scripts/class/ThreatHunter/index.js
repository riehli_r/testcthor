let assert = require('assert')
let fs = require('fs')
let Twitter = require('twitter')
let MongoUtil = require('../MongoUtil')
let Scraper = require('../Scraper')
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http, {"origins": "*"});
var app = express();

const globalConfig = require('../../config.json')

const constants = require('./constants.js')

class ThreatHunter {

  constructor() {
    this.websiteCallback = []
    this.mongoClient = new MongoUtil()
    this.mongoClient.connectToServer()

    this.client = new Twitter({
      consumer_key: '2pP2gXGtguvA0f36xNGhtLjlW',
      consumer_secret: 'aNUwsiPp8g6TbyvzvcuO61mMpr7pLJgtPY0BIzWS99FDULkoGL',
      access_token_key: '904640219609223170-69V2Mv5NacDmS8kYbWU5aWf8ScyBwH7',
      access_token_secret: 'dzAMvob9f2NDyRkFeA7Pk8a8xnOBV2E9ClMJIZsfAinzY'
    });

    this.containsFunc = [
      this.containsEmail,
      this.containsPhoneNumber,
      this.containsKeyWord,
      this.containsBitcoinAddress
    ];

    io.set("origins","*:*");
    io.on('connection', (socket) => {
      console.log("New user connected");
    });

    io.listen(3002);

    if (!fs.existsSync(constants.ARCHIVES_ROOT))
      fs.mkdirSync(constants.ARCHIVES_ROOT)
  }

  containsEmail(text) {
    let re = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    return re.test(text);
  }

  containsPhoneNumber(text) {
    let re = /\W[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}\W/i;
    return re.test(text);
  }

  containsKeyWord(text) {
    if (typeof(text) === 'undefined')
      return false
    globalConfig.keywords.forEach((keyword) => {
      const reg = new RegExp(keyword, "i")
      return text.search(reg) !== -1
    })
  }

  containsBitcoinAddress(text) {
    let re = /[13][a-km-zA-HJ-NP-Z1-9]{25,34}/;
    return re.test(text);
  }

  addWebsite(site) {
    if (!fs.existsSync(constants.ARCHIVES_ROOT + site.name))
      fs.mkdirSync(constants.ARCHIVES_ROOT + site.name)
    this.websiteCallback.push(site)
  }

  hunt(str) {
    console.log("hunting with ", str);
    this.client.stream('statuses/filter', {follow: '904640219609223170', track: str}, (stream) => {
      stream.on('data', (tweet) => {
        let display = false
        this.containsFunc.forEach((cb) => {
          if (cb(tweet.text) && !display) {
            display = true
            console.log(tweet.text)
            io.emit('tweet', tweet);
          }
        })
      });

      stream.on('error', (err) => {
        console.log(err);
      });
    });
  }

  saveInArchive(filepath, element) {
    if (fs.existsSync(constants.ARCHIVES_ROOT + filepath))
      return
    fs.writeFileSync(constants.ARCHIVES_ROOT + filepath, element.body)
    let db = this.mongoClient.getDb()
    let collection = db.collection('archives')

    collection.insert({filepath: filepath})
    Scraper.containsEmail(element, filepath, db)
    Scraper.containsPhoneNumber(element, filepath, db)
    Scraper.containsBitcoinWallet(element, filepath, db)
    Scraper.containsKeywords(element, filepath, db)
  }

  watch() {
    this.websiteCallback.forEach((el) => {
      if (typeof(el.cb) === 'function')
        el.cb(this)
    })
  }
}

module.exports = ThreatHunter
