const globalConfig = require('../../config.json')

module.exports = {
  containsEmail: (element, filepath, db) => {
    if (typeof(element) === 'undefined' || typeof(element.body) == 'undefined' || typeof(db) == 'undefined')
      return
    let re = element.body.match(/(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g)
    let collection = db.collection('data')
    if (re == null)
      return
    re.forEach((email) => {
      collection.insert({type: 'email', value: email, filepath: filepath, url: element.url})
    })
  },

  containsPhoneNumber: (element, filepath, db) => {
    if (typeof(element) === 'undefined' || typeof(element.body) == 'undefined' || typeof(db) == 'undefined')
      return
    let re = element.body.match(/(\+33|0|0033)[0-9]{9}/g)
    let collection = db.collection('data')
    if (re == null)
      return
    re.forEach((phone) => {
      collection.insert({type: 'phone', value: phone, filepath: filepath, url: element.url})
    })
  },

  containsBitcoinWallet: (element, filepath, db) => {
    if (typeof(element) === 'undefined' || typeof(element.body) == 'undefined' || typeof(db) == 'undefined')
      return
     let re = element.body.match(/[13][a-km-zA-HJ-NP-Z1-9]{25,34}/g)
     let collection = db.collection('data')
     if (re == null)
       return
     re.forEach((wallet) => {
       collection.insert({type: 'wallet', value: wallet, filepath: filepath, url: element.url})
     })
  },

  containsKeywords: (element, filepath, db) => {
    if (typeof(element) === 'undefined' || typeof(element.body) == 'undefined' || typeof(db) == 'undefined')
      return
    globalConfig.keywords.forEach((keyword) => {
      const reg = new RegExp(keyword, "i")
      if (element.body.search(reg) != -1) {
        let collection = db.collection('data')
        collection.insert({type: 'keyword', value: keyword, filepath: filepath, url: element.url})
      }
    })
  },
}
