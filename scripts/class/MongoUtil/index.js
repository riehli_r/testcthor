let MongoClient = require( 'mongodb' ).MongoClient
let config = require('./config')

var _db;

module.exports = class {

  connectToServer() {
    console.log("Try to connect to mongoDB")
    MongoClient.connect(config.url, function( err, db ) {
      if (err != null) {
        console.log(err)
        return
      }
      console.log("Connection to mongoDB database")
      _db = db;
    } );
  }

  getDb() {
    return _db;
  }
};
