let ThreatHunter = require('./class/ThreatHunter')

let sites = require('./sites')

let threatHunterInstance = new ThreatHunter()
threatHunterInstance.addWebsite(sites.pastebin)
threatHunterInstance.addWebsite(sites.blogHacker)
threatHunterInstance.addWebsite(sites.nopaste)
threatHunterInstance.addWebsite(sites.shadowLife)
threatHunterInstance.hunt('a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,1,2,3,4,5,6,7,8,9,0');

threatHunterInstance.watch()

setInterval(() => {threatHunterInstance.watch()}, 3600000)
