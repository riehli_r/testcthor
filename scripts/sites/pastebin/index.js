let request = require('request')
let Site = require ('../../class/Site')

function pastebin(threatHunter) {
  request('https://pastebin.com/trends', (error, response, body) => {

    if (error !== null || (response && response.statusCode !== 200))
      return

    let links = body.match(/<td>.+<a.+<\/td>/g)
    let ids = []
    links.forEach((el) => {
      let match = el.match(/<td>.+<a href="(.+)".+<\/td>/);
      if (typeof(match[1]) !== 'undefined') {
        const url = 'https://pastebin.com' + match[1]
        request(url, (err, res, body) => {
          if (!err) {
            let content = body.match(/<textarea id="paste_code".+>([\S\s]+)<\/textarea>/)
            if (typeof(content[1]) !== 'undefined')
              threatHunter.saveInArchive('pastebin' + match[1], {body: content[1], url: url})
          }
        })
      }
    })
  })
}

module.exports = new Site('pastebin', pastebin)
