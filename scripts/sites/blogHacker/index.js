let request = require('request')
var DomParser = require('dom-parser')
let Site = require ('../../class/Site')

function blogHacker(threatHunter) {
  request('https://www.leblogduhacker.fr/', (error, response, body) => {

    if (error !== null || (response && response.statusCode !== 200))
      return

    var parser = new DomParser();
    let doc = parser.parseFromString(body)
    doc.getElementsByClassName("more-link").forEach((link) => {
      const url = link.getAttribute("href")
      const name = link.getAttribute('href').substr(30).slice(0, -1)
      request(url, (err, response, body) => {
        let article = parser.parseFromString(body)
        let divContent = article.getElementsByClassName('entry')[0]
        let content = divContent.innerHTML
        content = content.replace(/<[^<>]+>/g, '')
        threatHunter.saveInArchive('leblogduhacker/'  + name, {body: content, url: url})
        let commentList = article.getElementsByClassName('commentlist')[0]
        if (typeof(commentList) !== 'undefined') {
          let comments = commentList.getElementsByTagName('li')
          comments.forEach((comment) => {
            let commentDiv = comment.getElementsByTagName('div')[0];
            if (typeof(commentDiv) !== 'undefined') {
              let id = commentDiv.getAttribute('id')
              let commentBody = commentDiv.getElementsByTagName('div')[2]
              if (typeof(commentBody) !== 'undefined') {
                let commentContent = commentBody.innerHTML.replace(/<[^<>]+>/g, '')
                threatHunter.saveInArchive('leblogduhacker/'  + id, {body: commentContent, url: url})

              }
            }
          })
        }
      })
    })

  })
}

module.exports = new Site('leblogduhacker', blogHacker)
