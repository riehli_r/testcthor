let tr = require('tor-request')
var DomParser = require('dom-parser')
let Site = require ('../../class/Site')

function shadowLife(threatHunter) {
  tr.request('http://shadow7jnzxjkvpz.onion/', (error, response, body) => {

    if (error !== null || (response && response.statusCode !== 200))
      return

    var parser = new DomParser();
    let doc = parser.parseFromString(body)
    doc.getElementsByClassName('more-link').forEach((link) => {
      url = link.getAttribute('href').slice(0, -1)
      tr.request('http://shadow7jnzxjkvpz.onion' + url, (err, res, body) => {
        if (err) {
          console.log(err);
          return;
        }
        let article = parser.parseFromString(body)
        let entry = article.getElementsByClassName('entry')[0]
        if (typeof(entry) !== 'undefined') {
          let content = entry.innerHTML.replace(/<[^<>]+>/g, '')
          let path = content.substr(48, 10).replace(/[ -]/g, '')
          threatHunter.saveInArchive('shadowLife/'  + path, {body: content, url: url})
        }
      })
    })
  })
}

module.exports = new Site('shadowLife', shadowLife)
