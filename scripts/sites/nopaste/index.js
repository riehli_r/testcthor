let request = require('request')
var DomParser = require('dom-parser')
let Site = require ('../../class/Site')

function nopaste(threatHunter) {
  request('https://nopaste.me/trends', (error, response, body) => {

    if (error)
      console.log("totototototototototootototototo");
      else {
        var parser = new DomParser();
        let doc = parser.parseFromString(body)

        let table = doc.getElementsByTagName('tbody')[0]
        if (typeof(table) !== 'undefined') {
          doc.getElementsByTagName('tr').forEach((line) => {
            let link = line.getElementsByTagName('a')[0]
            if (typeof(link) !== 'undefined') {
              let url = link.getAttribute('href')
              let id = url.substr(-8)
              request('https://nopaste.me/view/raw/' + id, (err, res, body) => {
                threatHunter.saveInArchive('nopaste/'  + id, {body: body, url: url})

              })
            }
          })
      }
    }
  })
}

module.exports = new Site('nopaste', nopaste)
