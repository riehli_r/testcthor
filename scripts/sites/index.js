module.exports = {
  pastebin: require('./pastebin'),
  blogHacker: require('./blogHacker'),
  nopaste: require('./nopaste'),
  shadowLife: require('./shadowLife')
}
