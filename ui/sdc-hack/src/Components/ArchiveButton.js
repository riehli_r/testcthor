import React from 'react';
import ArchiveModal from './ArchiveModal';
import request from 'request';

class ArchiveButton extends React.Component {

  state = {
    isShowingModal: false,
    content: '',
  }

  readTextFile = (file) => {
    request('http://localhost:3001/file?filepath=/home/riehli_r/Documents/ETNA/SDC/HACK/scripts/archives/' + file, (err, res, body) => {
      this.setState({content: body});
    })
  }

  handleClick = () => {
    console.log("CLICK ", this.props.filepath)
    this.readTextFile(this.props.filepath)
    this.setState({isShowingModal: true})
  }

  handleClose = () => this.setState({isShowingModal: false})

  render() {
    return <a style={{color: 'green'}} onClick={this.handleClick}>
      <span>{this.props.link}</span>
      {
        this.state.isShowingModal &&
        <ArchiveModal archive={this.props.filepath} content={this.state.content} onClose={this.handleClose}/>
      }
    </a>;
  }
}

export default ArchiveButton;
