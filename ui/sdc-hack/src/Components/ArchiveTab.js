import React, { Component, PureComponent } from 'react';
import request from 'request';
import ArchiveButton from './ArchiveButton';

class Archive extends PureComponent {
  render() {
    return (
      <div className="archive">
        <img style={{width: '50px',}} src="https://image.flaticon.com/icons/svg/148/148964.svg" />
        <div className="archiveName">{this.props.filepath}</div>
      </div>
    );
  }
}

class ArchiveTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      archives : [],
    }
  }

  componentWillMount() {
    request('http://localhost:3001/archives', (err, res, body) => {
      if (!err) {
        const archives = JSON.parse(body);
        console.log(archives);
        this.setState({archives});
      }
    })
  }

  renderList = () => (
    this.state.archives.map((archive, i) => (
      <ArchiveButton  key={i} filepath={archive.filepath} link={<Archive filepath={archive.filepath}/>} />
    ))
  )

  render() {
    return(
      <div className="archiveContainer">{this.renderList()}</div>
    );
  }
}
export default ArchiveTab;
