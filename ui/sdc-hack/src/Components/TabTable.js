import React, { Component } from 'react';
import Table from './Table';

class TabTable extends Component {

  constructor(props) {
    super(props);

    this.filterSearchChangeCallback = null;
    this.filterTypeChangeCallback = null;
    this.filterSearchChange = this.filterSearchChange.bind(this);
    this.filterTypeChange = this.filterTypeChange.bind(this);
  }

  filterSearchChange(event) {
    if (this.filterSearchChangeCallback)
      this.filterSearchChangeCallback(event);
  }

  filterTypeChange(event) {
    if (this.filterTypeChangeCallback)
      this.filterTypeChangeCallback(event)
  }

  render() {
    return(
      <div>
        <select onChange={this.filterTypeChange}>
          <option value="all">Tout</option>
          <option value="email">Email</option>
          <option value="phone">Telephone</option>
          <option value="wallet">Wallet</option>
          <option value="keyword">Keyword</option>
        </select>
        <input type="text" onChange={this.filterSearchChange}/>
        <Table setFilterSearchCallback={(cb) => { this.filterSearchChangeCallback = cb; }} setFilterTypeCallback={(cb => { this.filterTypeChangeCallback = cb; })}></Table>

      </div>
    );
  }
}
export default TabTable;
