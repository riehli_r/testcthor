import React, { Component } from 'react';
import request from 'request';
import ArchiveButton from './ArchiveButton';

class Table extends Component {

  constructor(props) {
    super(props);

    this.state = {
      elements: [],
      searchFilter: "",
      typeFilter: "all",
    };
    this.filterSearchChange = this.filterSearchChange.bind(this);
    this.filterTypeChange = this.filterTypeChange.bind(this);
    this.props.setFilterSearchCallback(this.filterSearchChange);
    this.props.setFilterTypeCallback(this.filterTypeChange);
  }

  filterSearchChange(event) {
    this.setState({searchFilter: event.target.value});
    this.getDataFromApi(event.target.value, this.state.typeFilter);
  }

  filterTypeChange(event) {
    this.setState({typeFilter: event.target.value});
    this.getDataFromApi(this.state.searchFilter, event.target.value);
  }

  componentDidMount() {
    this.getDataFromApi("", 'all');
  }

  getDataFromApi(searchFilter, typeFilter) {

    request('http://localhost:3001/datas?search=' + searchFilter, (err, res, body) => {
      if (!err) {
        let elementsRow = []
        let elements = JSON.parse(body);

        elements.forEach((el) => {
          if (typeFilter === 'all' || typeFilter === el.type) {
            elementsRow.push(<tr>
              <td>{el.type}</td>
              <td>{el.value}</td>
              <td><a href={el.url}>{el.url}</a></td>
              <td><ArchiveButton  link={el.filepath} filepath={el.filepath} /></td>
            </tr>
          )
        }
      })

        this.setState({elements: elementsRow});
      }
    })
  }

  render() {
    return(
      <div>
        <table className="container">
          <tr className="header">
            <th className="type"><h1>Type</h1></th>
            <th><h1>Valeur</h1></th>
            <th><h1>Lien</h1></th>
            <th><h1>Archive</h1></th>
          </tr>
          { this.state.elements }
        </table>
      </div>
    );
  }
}

export default Table;
