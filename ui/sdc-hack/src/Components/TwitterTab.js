import React, { Component } from 'react';
import socket from 'socket.io-client';
import config from '../config.json';



export function createMarkup(data) {
  return {__html: data.replace(/(\W[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}\W)|((\+33|0|0033)[0-9]{9})|((([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})))|([13][a-km-zA-HJ-NP-Z1-9]{25,34})/g, "<span class='highlighted'>$1$2$3$4</span>")};
}

class Tweet extends Component {

  render() {
    let tweet = this.props.tweet;
    return (
      <div className="tweet">
        <div className="tweetHeader">
          <div className="tweetImg">
            <img src={tweet.user.profile_image_url}/>
          </div>
          <div className="tweetNameContainer">
            <p className="tweetName">{tweet.user.name}</p>
            <p className="tweetScreenName">{'@' + tweet.user.screen_name}</p>
          </div>
        </div>
        <div className="tweetContent" dangerouslySetInnerHTML={createMarkup(tweet.text)} />
      </div>
    );
  }
}

class TwitterTab extends Component {

  renderList = () => (
    this.props.tweets.map((tweet, i) => (
      <Tweet key={i} tweet={tweet} />
    ))
  )

  render() {
    return(
      <div className="tweetsContainer">
        {this.renderList()}
      </div>
    );
  }
}
export default TwitterTab;
