import React from 'react';
import {ModalContainer, ModalDialog} from 'react-modal-dialog';
import ReactSpinner from 'react-spinjs';

const highlightEmail = (match, offset, string) => {
  return ("<span class='highlighted_email'>" + match + "</span>");
}

const highlightPhone = (match, offset, string) => {
  return ("<span class='highlighted_phone'>" + match + "</span>");
}

const highlightWallet = (match, offset, string) => {
  return ("<span class='highlighted_wallet'>" + match + "</span>");
}

const highlightText = (data) => {
  var highlighted = data.replace(/((([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})))/g, highlightEmail);
  highlighted = highlighted.replace(/((\+33|0|0033)[0-9]{9})/g, highlightPhone);
  highlighted = highlighted.replace(/([13][a-km-zA-HJ-NP-Z1-9]{25,34})/g, highlightWallet);
  return {__html: highlighted};
}

class ArchiveModal extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        isLoading: true,
      }
  }

  componentWillReceiveProps(nextProps) {
      if (nextProps.content !== "")
        this.setState({isLoading: false});
  }

  render() {
    return (
        <ModalContainer onClose={this.props.onClose}>
          <ModalDialog onClose={this.props.onClose}>
            <h1 className="modalTitle">{this.props.archive}</h1>
            {this.state.isLoading ?
              <ReactSpinner/> :
              <pre>
                <div className="modalText" dangerouslySetInnerHTML={highlightText(this.props.content)} />
              </pre>}
          </ModalDialog>
        </ModalContainer>
        // <p className="modalText">{this.props.content}</p>
      )
  }
}

export default ArchiveModal;
