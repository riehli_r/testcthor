import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import logo from './logo.png';
import TabTable from './Components/TabTable';
import TwitterTab from './Components/TwitterTab';
import ArchiveTab from './Components/ArchiveTab';
import './App.css';

import 'react-tabs/style/react-tabs.css';
import socket from 'socket.io-client';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tweets : [],
    }
  }

  componentWillMount() {
    if (this.state.tweets.length == 0) {
      this.client = socket('http://localhost:3002');
      this.client.on('tweet', (tweet) => {
        console.log(tweet);
        this.setState({tweets:[tweet, ...this.state.tweets]});
      });
    }
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Threat hunter</h2>
        </div>
        <Tabs>
          <TabList>
            <Tab>Sites web</Tab>
            <Tab>Twitter</Tab>
            <Tab>Archives</Tab>
          </TabList>
          <TabPanel>
            <TabTable />
          </TabPanel>
          <TabPanel>
            <TwitterTab tweets={this.state.tweets}/>
          </TabPanel>
          <TabPanel>
            <ArchiveTab />
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

export default App;
