const Twitter = require('twitter');

const client = new Twitter({
  consumer_key: '2pP2gXGtguvA0f36xNGhtLjlW',
  consumer_secret: 'aNUwsiPp8g6TbyvzvcuO61mMpr7pLJgtPY0BIzWS99FDULkoGL',
  access_token_key: '904640219609223170-69V2Mv5NacDmS8kYbWU5aWf8ScyBwH7',
  access_token_secret: 'dzAMvob9f2NDyRkFeA7Pk8a8xnOBV2E9ClMJIZsfAinzY'
});

const containsFunc = [
  containsEmail,
  containsPhoneNumber,
  containsKeyWord,
  containsBitcoinAddress
];

function hunt(str) {
  client.stream('statuses/filter', {follow: '904640219609223170', track: str}, (stream) => {
    stream.on('data', (tweet) => {
      for (let i = containsFunc.length - 1; i >= 0; i--) {
        if (containsFunc[i](tweet.text)) {
          console.log(tweet.text, '\n---------------------');
          break;
        }
      }
    });

    stream.on('error', (err) => {
      console.log(err);
    });
  });
}

function containsEmail(text) {
  let re = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
  return re.test(text);
}

function containsPhoneNumber(text) {
  let re = /\W[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}\W/i;
  return re.test(text);
}

function containsKeyWord(text) {
  let re = /\W?hack\W/i;
  return re.test(text);
}

function containsBitcoinAddress(text) {
  let re = /[13][a-km-zA-HJ-NP-Z1-9]{25,34}/;
  return re.test(text);
}

hunt('e');