let MongoClient = require( 'mongodb' ).MongoClient
let fs = require('fs')
var express = require('express');
var http = require('http').Server(app);
var io = require('socket.io')(http, {"origins": "*"});
var app = express();

app.get('/datas', (req, res) => {

  MongoClient.connect("mongodb://localhost:27017/threathunter", (err, db) => {
    let collection = db.collection('data');

    if (err != null) {
      console.log(err)
      return
    }
    console.log("Connection to mongoDB database")
    console.log(req.query.search);
    let opt = null
    if (typeof req.query.search !== 'undefined' && req.query.search !== '') {
      let reg = new RegExp(req.query.search, 'i');
      opt = {value: {$regex: reg}};
    }
    collection.find(opt).toArray((err, docs) => {
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.send(docs);
    });
  });
});

app.get('/archives', (req, res) => {
  MongoClient.connect("mongodb://localhost:27017/threathunter", (err, db) => {
    let collection = db.collection('archives');

    if (err != null) {
      console.log(err)
      return
    }
    console.log("Connection to mongoDB database")
    collection.find().toArray((err, docs) => {
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.send(docs);
    });
  });
});

app.get('/file', (req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  fs.readFile(req.query.filepath, 'utf8', (err, data) => {
    if (err)
      res.json({error: 'file not found'})
    else
      res.send(data)
  })
})


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});



app.listen(3001);
